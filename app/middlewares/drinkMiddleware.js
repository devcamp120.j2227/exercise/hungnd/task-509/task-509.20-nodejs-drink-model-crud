const getAllDrinksMiddleware = (req, res, next) => {
    console.log("Get All Drinks!");
    next();
};

const getDrinksMiddleware = (req, res, next) => {
    console.log("Get a Drinks!");
    next();
};

const postDrinksMiddleware = (req, res, next) => {
    console.log("Create a Drinks!");
    next();
};

const putDrinksMiddleware = (req, res, next) => {
    console.log("Update a Drinks!");
    next();
};

const deleteDrinksMiddleware = (req, res, next) => {
    console.log("Delete a Drinks!");
    next();
};
module.exports = {
    getAllDrinksMiddleware,
    getDrinksMiddleware,
    postDrinksMiddleware,
    putDrinksMiddleware,
    deleteDrinksMiddleware
}