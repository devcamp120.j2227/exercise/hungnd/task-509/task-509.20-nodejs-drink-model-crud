const getAllVouchersMiddleware = (req, res, next) => {
    console.log("Get All Vouchers!");
    next();
};

const getVouchersMiddleware = (req, res, next) => {
    console.log("Get a Vouchers!");
    next();
};

const postVouchersMiddleware = (req, res, next) => {
    console.log("Create a Vouchers!");
    next();
};

const putVouchersMiddleware = (req, res, next) => {
    console.log("Update a Vouchers!");
    next();
};

const deleteVouchersMiddleware = (req, res, next) => {
    console.log("Delete a Vouchers!");
    next();
};
module.exports = {
    getAllVouchersMiddleware,
    getVouchersMiddleware,
    postVouchersMiddleware,
    putVouchersMiddleware,
    deleteVouchersMiddleware
}