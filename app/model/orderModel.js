// Import thư viện mongoose
const mongoose = require("mongoose");
// class Schema từ thư viện mongoose
const Schema =  mongoose.Schema;

// Khởi tạo instance orderSchema từ class Schema
const orderSchema = new Schema({
    orderCode: {
        type: String,
        unique: true
    },
    pizzaSize: {
        type: String,
        required: true
    },
    pizzaType: {
        type: String,
        required: true
    },
    voucher: [{
        type: Schema.Types.ObjectId,
        ref: "Voucher"
    }],
    drink: [{
        type: Schema.Types.ObjectId,
        ref: "Drink"
    }],
    status: {
        type: String,
        required: true
    }
});
module.exports = mongoose.model("Order", orderSchema);