// Import thư viện mongoose
const mongoose = require("mongoose");
// class Schema từ thư viện mongoose
const Schema =  mongoose.Schema;

// Khởi tạo instance userSchema từ class Schema
const userSchema = new Schema({
    fullName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    adress: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
        unique: true
    },
    orders: [{
        type: mongoose.Types.ObjectId,
        ref: "Order"
    }]
});
module.exports = mongoose.model("User", userSchema);