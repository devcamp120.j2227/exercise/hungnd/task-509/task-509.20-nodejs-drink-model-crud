// Import thư viện mongoose
const mongoose = require("mongoose");
// class Schema từ thư viện mongoose
const Schema =  mongoose.Schema;

// Khởi tạo instance drinkSchema từ class Schema
const drinkSchema = new Schema({
    maNuocUong: {
        type: String,
        unique: true,
        required: true
    },
    tenNuocUong: {
        type: String,
        required: true,
    },
    donGia: {
        type: Number,
        required: true
    }
});
module.exports = mongoose.model("Drink", drinkSchema);