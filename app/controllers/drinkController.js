// khai báo thư viện mongoose
const mongoose = require("mongoose");
// Khai báo models
const drinkModel = require("../model/drinkModel");

// Create a drink
const createDrink = (req, res) => {
    //B1: Thu thập dữ liệu
    let bodyDrink = req.body;
    console.log(bodyDrink);
    //B2: Kiểm tra dữ liệu
    // Kiểm tra maNuocUong
    if (!bodyDrink.maNuocUong) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "maNuocUong is not valid!"
        });
    }
    // Kiểm tra tenNuocUong
    if (!bodyDrink.tenNuocUong) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "tenNuocUong is not valid!"
        })
    }
    //Kiểm tra donGia
    if (!bodyDrink.donGia || bodyDrink.donGia < 0) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "donGia is not valid!"
        })
    }
    //B3: thực hiện tạo mới drink
    let newDrink = {
        _id: mongoose.Types.ObjectId(),
        maNuocUong: bodyDrink.maNuocUong,
        tenNuocUong: bodyDrink.tenNuocUong,
        donGia: bodyDrink.donGia
    }

    drinkModel.create(newDrink, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Create new drink successfully!",
                "data": data
            })
        }
    })
};

// Get all drinks
const getAllDrink = (req, res) => {
    //B1: Thu thập dữ liệu (không cần)
    //B2: Kiểm tra dữ liệu (không cần)
    //B3: Thực hiện load all drink
    drinkModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Load all drinks successfully!",
                "data": data
            })
        }
    })
};
// get a drink by id
const getDrinkById = (req, res) => {
    //B1: Thu thập dữ liệu
    const drinkId = req.params.drinkId;
    console.log(drinkId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Drink Id is not valid!"
        })
    }
    // B3: Thực hiện load course theo id
    drinkModel.findById(drinkId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Get drink by Id successfully!",
                "data": data
            })
        }
    })
};
// Update a drink
const updateDrinkById = (req, res) => {
    //B1: Thu thập dữ liệu
    const drinkId = req.params.drinkId;
    console.log(drinkId);
    let bodyDrink = req.body;
    console.log(bodyDrink);
    //B2: Kiểm tra dữ liệu
    // KIểm tra course Id
    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Drink Id is not valid!"
        })
    }
    // Kiểm tra maNuocUong
    if (!bodyDrink.maNuocUong) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "maNuocUong is not valid!"
        });
    }
    // Kiểm tra tenNuocUong
    if (!bodyDrink.tenNuocUong) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "tenNuocUong is not valid!"
        })
    }
    //Kiểm tra donGia
    if (!bodyDrink.donGia || bodyDrink.donGia < 0) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "donGia is not valid!"
        })
    }
    //B3: thực hiện tạo mới drink
    let newDrink = {
        maNuocUong: bodyDrink.maNuocUong,
        tenNuocUong: bodyDrink.tenNuocUong,
        donGia: bodyDrink.donGia
    }
    drinkModel.findByIdAndUpdate(drinkId, newDrink, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Update drink by Id successfully!",
                "data": data
            })
        }
    })
};
// delete a drink
const deleteDrinkById = (req, res) => {
    //B1: Thu thập dữ liệu
    const drinkId = req.params.drinkId;
    console.log(drinkId);
    //B2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
        return res.status(400).json({
            "status": "Error 400: bad request",
            "message": "Drink Id is not valid!"
        })
    }
    // B3: Thực hiện xóa drink theo id
    drinkModel.findByIdAndDelete(drinkId, (err, data) => {
        if (err) {
            return res.status(500).json({
                "status": "Error 500: internal server error",
                "message": err.message
            })
        }
        else {
            return res.status(201).json({
                "status": "Delete drink by Id successfully!",
                "data": data
            })
        }
    })
}
module.exports = {
    createDrink,
    getAllDrink,
    getDrinkById,
    updateDrinkById,
    deleteDrinkById
}
